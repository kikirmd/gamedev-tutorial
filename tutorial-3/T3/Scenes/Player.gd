extends KinematicBody2D

export (int) var speed = 400
export (int) var run_speed = 1000
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600	

const UP = Vector2(0,-1)

var velocity = Vector2()
var run = false;
var jump_count = 0;
var dashing          = false   # whether player is dash or not
var min_dash_delay   = 0.1     # time between keypress (.5 secs)
var max_dash_delay   = 1.0
var max_dash_time    = 2.0     # how long player can run for (2 secs)
var dash_timer       = 0       # timer to reset dash state
var previous_action="up"
var elapsed_time =0
var current_time=0
var prev_time=0


func _ready():
	pass # Replace with function body.


func get_input(delta):
	velocity.x = 0
#	if is_on_floor() and Input.is_action_just_pressed('up'):
#		velocity.y = jump_speed
	if Input.is_action_just_pressed('up') and jump_count<2:
		velocity.y = jump_speed		
		jump_count +=1
#	if run is false
#	print(run)
	if Input.is_action_pressed('right') and not run:
		previous_action = "right"
		velocity.x += speed
		
		if previous_action == "right" and dash_timer<max_dash_delay and dash_timer>min_dash_delay:
			run = true
		else:
			run = false
#
	if Input.is_action_pressed('right') and run:
		velocity.x += run_speed
		
	if Input.is_action_pressed('left') and not run:
		previous_action = "left"
		velocity.x -= speed
		
		if previous_action == "left" and dash_timer<max_dash_delay and dash_timer>min_dash_delay:
			run = true
		else:
			run = false
	if Input.is_action_pressed('left') and run:
		velocity.x -= run_speed

func _physics_process(delta):
#func _process(delta):
	elapsed_time+= delta
#	print("del")
#	print(delta)
#	print("dash")
#	print(dash_timer)
	print(run)
	velocity.y += delta * GRAVITY
	if Input.is_action_just_pressed('right') and not run:
		if previous_action == "right":
			prev_time = current_time
			current_time = elapsed_time
			dash_timer = current_time-prev_time
			if dash_timer<max_dash_delay and dash_timer>min_dash_delay:
				run =true
				print("run")
	if Input.is_action_just_pressed('left') and not run:
		if previous_action == "left":
			prev_time = current_time
			current_time = elapsed_time
			dash_timer = current_time-prev_time
			if dash_timer<max_dash_delay and dash_timer>min_dash_delay:
				run =true
				print("run")
	get_input(delta)
	velocity = move_and_slide(velocity, UP)
	if is_on_floor():
		jump_count = 0
	if Input.is_action_just_released("left"):
			if run == true:
				run = false
				dash_timer=0
		
	if Input.is_action_just_released("right"):
		if run == true:
			run = false
			dash_timer=0

# Called when the node enters the scene tree for the first time.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
