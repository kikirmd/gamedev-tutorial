extends KinematicBody2D

export (int) var speed = 500
export (int) var GRAVITY = 1200
export (int) var jump_speed = -550
export (int) var run_speed = 800
var jump_count = 0;

const UP = Vector2(0,-1)

var velocity = Vector2()

var run = false;
var dashing          = false   # whether player is dash or not
var min_dash_delay   = 0.1     # time between keypress (.5 secs)
var max_dash_delay   = 1.0
var max_dash_time    = 2.0     # how long player can run for (2 secs)
var dash_timer       = 0       # timer to reset dash state
var previous_action="up"
var elapsed_time =0
var current_time=0
var prev_time=0


onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

func get_input():
#	velocity.x = 0
#	if Input.is_action_just_pressed('jump') and jump_count<2: 
#		jump_count+=1
#		velocity.y = jump_speed
#	if Input.is_action_pressed('right'):
#		velocity.x += speed
#	if Input.is_action_pressed('left'):
#		velocity.x -= speed

	velocity.x = 0
#	to limit the jumpt to 2 consecutive jumps
	if Input.is_action_just_pressed('up') and jump_count<2:
		velocity.y = jump_speed		
		jump_count +=1

	if Input.is_action_pressed('right') and not run:
		previous_action = "right"
		velocity.x += speed
		
		if previous_action == "right" and dash_timer<max_dash_delay and dash_timer>min_dash_delay:
			run = true
		else:
			run = false
#
	if Input.is_action_pressed('right') and run:
		velocity.x += run_speed
		
	if Input.is_action_pressed('left') and not run:
		previous_action = "left"
		velocity.x -= speed
		
		if previous_action == "left" and dash_timer<max_dash_delay and dash_timer>min_dash_delay:
			run = true
		else:
			run = false
	if Input.is_action_pressed('left') and run:
		velocity.x -= run_speed

func _physics_process(delta):
	elapsed_time+= delta
	velocity.y += delta * GRAVITY
	if Input.is_action_just_pressed('right') and not run:
		if previous_action == "right":
			prev_time = current_time
			current_time = elapsed_time
			dash_timer = current_time-prev_time
			if dash_timer<max_dash_delay and dash_timer>min_dash_delay:
				run =true
	if Input.is_action_just_pressed('left') and not run:
		if previous_action == "left":
			prev_time = current_time
			current_time = elapsed_time
			dash_timer = current_time-prev_time
			if dash_timer<max_dash_delay and dash_timer>min_dash_delay:
				run =true
	get_input()
	velocity = move_and_slide(velocity, UP)
	if is_on_floor():
		jump_count = 0
	if Input.is_action_just_released("left"):
			if run == true:
				run = false
				dash_timer=0
		
	if Input.is_action_just_released("right"):
		if run == true:
			run = false
			dash_timer=0

#	velocity.y += delta * GRAVITY
#	get_input()
#	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		animator.play("Idle")
		jump_count=0
	
